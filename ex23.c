/*
 * =====================================================================================
 *
 *       Filename:  ex23.c
 *
 *    Description:  Meet Duff's Device
 *
 *        Version:  1.0
 *        Created:  12/11/2012 06:58:30 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Ji.Zhilong
 *   Organization:  
 *
 * =====================================================================================
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "dbg.h"
#define DEVICE8(count, expr) do{ int n =((count) + 7)/8;\
                                 switch((count) % 8) {\
									case 0: do{expr;\
											  case 7:expr;\
											  case 6:expr;\
											  case 5:expr;\
											  case 4:expr;\
											  case 3:expr;\
											  case 2:expr;\
											  case 1:expr;\
											}while(--n>0);}}while(0)




int normal_copy(char *from, char *to, int count)
{
  int i = 0;

  for(i = 0; i < count; i++) {
	to[i] = from[i];
  }

  return i;
}

int duffs_device(char *from, char *to, int count)
{
 {
	int n = (count + 7) / 8;
	
	switch(count % 8) {
	  case 0: do { *to++ = *from++;
				 case 7: *to++ = *from++;
				 case 6: *to++ = *from++;
				 case 5: *to++ = *from++;
				 case 4: *to++ = *from++;
				 case 3: *to++ = *from++;
				 case 2: *to++ = *from++;
				 case 1: *to++ = *from++;
			  } while(--n > 0);
	}
 }
//  DEVICE8(count,*to++=*from++);
  return count;
}

int zeds_device(char *from, char *to, int count)
{
  {
	int n = (count + 7) / 8;

	switch(count % 8) {
	  case 0:
again: *to++ = *from++;
	  case 7: *to++ = *from++;
	  case 6: *to++ = *from++;
	  case 5: *to++ = *from++;
	  case 4: *to++ = *from++;
	  case 3: *to++ = *from++;
	  case 2: *to++ = *from++;
	  case 1: *to++ = *from++;
			  if(--n > 0) goto again;
	}
  }

  return count;
}

int valid_copy(char *data, int count, char expects)
{
  int i = 0;
  for(i = 0; i < count; i++) {
	if(data[i] != expects) {
	  log_err("[%d] %c != %c", i, data[i], expects);
	  return 0;
	}
  }

  return 1;
}

void compare_speed()
{
	char *from = malloc(sizeof(char)*1000);
	char *to = malloc(sizeof(char)*1000);
	const int LOOPS  = 1000000;
	int i;
	clock_t start;

	// set up;
	memset(from, 'a', 1000);
	memset(to, 'x', 1000);

	// time the normal;
	start = clock();
	for(i = 0; i < LOOPS; i++) {
	  normal_copy(from, to, 100);
	}
	start = clock() -start;
	printf("normal copy used: %ld\n",start);

	// time duffs_device
	start = clock();
	for(i = 0; i < LOOPS; i++) {
	  duffs_device(from, to, 100);
	}
	start = clock() -start;
	printf("Duff's device used: %ld\n",start);
	free(from);
	free(to);
	
}

int main(int argc, char *argv[])
{
  char from[1000] = {'a'};
  char to[1000] = {'c'};
  int rc = 0;

  // setup the from to have some stuff
  memset(from, 'x', 1000);
  // set it to a failure mode
  memset(to, 'y', 1000);
  check(valid_copy(to, 1000, 'y'), "Not initialized right.");

  // use normal copy to 
  rc = normal_copy(from, to, 1000);
  check(rc == 1000,"Normal copy failed: %d", rc);
  check(valid_copy(to, 1000, 'x'), "Normal copy failed.");

  //reset
  memset(to, 'y', 1000);

  // duffs version
  rc = duffs_device(from, to, 1000);
  check(rc == 1000,"Duff's device failed: %d", rc);
  check(valid_copy(to, 1000, 'x'), "Duff's device failed copy.");

  //reset
  memset(to, 'y', 1000);

  // my version
  rc = zeds_device(from, to, 1000);
  check(rc == 1000,"Zed's device failed: %d", rc);
  check(valid_copy(to, 1000, 'x'), "Zed's device failed copy.");

  compare_speed();

  return 0;
error:
  return 1;
}

