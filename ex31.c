/*
 * =====================================================================================
 *
 *       Filename:  ex31.c
 *
 *    Description:	Debugging Code  
 *
 *        Version:  1.0
 *        Created:  12/18/2012 09:03:11 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Ji.Zhilong
 *   Organization:  
 *
 * =====================================================================================
 */
#include <unistd.h>

int main(int argc, char *argv[])
{
  int i = 0;

  while(i < 100) {
	usleep(3000);
  }

  return 0;
}


