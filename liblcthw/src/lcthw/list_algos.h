#include "list.h"

typedef int (*List_compare)(const void *val1, const void *val2);

int List_bubble_sort(List *list, List_compare cmp);

List *List_merge_sort(List *list, List_compare cmp);

int List_insert_sorted(List *list, void *value, List_compare cmp);

