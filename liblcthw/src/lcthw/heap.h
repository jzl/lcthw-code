#ifndef __heap__h_
#define __heap__h_
#include <stdlib.h>
#include <string.h>

static inline void swap(void *a, void *b, size_t size)
{
  void *temp = malloc(size);
  memcpy(temp, b, size);
  memcpy(b, a, size);
  memcpy(a, temp, size);
  free(temp);
}
void Max_Heapify(void *base, unsigned int i, size_t el_size, size_t heap_size,
				 int(*cmp)(const void *, const void *));

void Build_Max_Heap(void *base, size_t el_size, size_t heap_size,
					int(*cmp)(const void *, const void *));

void HeapSort(void *base, size_t el_size, size_t heap_size, 
			  int(*cmp)(const void *, const void *));

#endif
