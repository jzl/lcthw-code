#include "list_algos.h"

inline static void List_swap(ListNode *node1, ListNode *node2)
{
  void *tmp = node1->value;
  node1->value = node2->value;
  node2->value = tmp;
}

int List_bubble_sort(List *list, List_compare cmp)
{
  ListNode *up,*cur;
  for(up=list->last;up && up != list->first;up = up->prev) {
	for(cur=list->first;cur && cur != up; cur = cur->next) {
	  if(cmp(cur->value, cur->next->value) > 0) {
		List_swap(cur, cur->next);
	  }
	}
  }
  return 0;

}

List *List_merge(List *left, List *right, List_compare cmp)
{
  ListNode *curl,*curr,*curm;
  curr = right->first;
  for(curm=curl=left->first; curl; curl= curm) {
	curm = curm->next;
	for(;curr && cmp(curr->value,curl->value) <= 0;curr=curr->next);
	if(curr) {
	  List_insert_before(right,curr, curl);
	} else {
	  List_insert_after(right,right->last,curl);
	  curr = curl;
	}
  }
  

  free(left);
  return right;
  
}

int List_insert_sorted(List *list, void *value, List_compare cmp)
{
  if(List_emptyp(list)) {
	List_push(list, value);
	return 0;
  }
  ListNode *cur = NULL;
  for(cur = list->first; cur && cmp(cur->value, value) <= 0; cur = cur->next);
  if(cur) {
	ListNode *node = calloc(1, sizeof(ListNode));
	node->value = value;
	List_insert_before(list, cur, node);
  } else {
	List_push(list, value);
  }

  return 0;
}

List *List_merge_sort(List *list, List_compare cmp)
{
  if(List_count(list) <= 1)
	return List_copy(list);
  List *left = List_create();
  List *right = List_create();
  List_split(list, List_count(list) / 2, left, right);

  List *sort_left = List_merge_sort(left, cmp);
  List *sort_right = List_merge_sort(right, cmp);

  free(left);
  free(right);


  return List_merge(sort_left, sort_right, cmp);
}

