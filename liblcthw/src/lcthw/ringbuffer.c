#undef NDEBUG
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <lcthw/dbg.h>
#include <lcthw/ringbuffer.h>

#define MIN(A,B) ((A) > (B) ? (B) : (A))

RingBuffer *RingBuffer_create(int length)
{
  RingBuffer *buffer = calloc(1, sizeof(RingBuffer));
  buffer->length = length;
  buffer->start = 0;
  buffer->end = 0;
  buffer->buffer = calloc(buffer->length, 1);

  return buffer;
}

void RingBuffer_destroy(RingBuffer *buffer)
{
  if(buffer) {
	free(buffer->buffer);
	free(buffer);
  }
}

int RingBuffer_write(RingBuffer *buffer, char *data, int length)
{
  if(RingBuffer_empty(buffer)) {
	buffer->start = buffer->end = 0;
  }

  check(length <= RingBuffer_available_space(buffer),
	    "Not enough space: %d request, %d available",
		length, RingBuffer_available_space(buffer));

  int len_front, len_tail;
  len_tail = MIN(length, buffer->length - (buffer->end % buffer->length));
  len_front = length - len_tail;
  void *result = memcpy(RingBuffer_ends_at(buffer), data, len_tail);
  check(result != NULL, "Failed to write data into buffer.");

  if(len_front > 0) {
	result = memcpy(buffer->buffer, data + len_tail, len_front);
	check(result != NULL, "Failed to write data into buffer.");
  }

  RingBuffer_commit_write(buffer, length);

  return length;
error:
  return -1;
}

int RingBuffer_read(RingBuffer *buffer, char *target, int amount)
{
  check_debug(amount <= RingBuffer_available_data(buffer),
	    "Not enough space: %d request, %d available",
		amount, RingBuffer_available_data(buffer));

  int len_tail, len_front;
  len_tail = MIN(amount, buffer->length-(buffer->start % buffer->length));
  len_front = amount - len_tail;

  void *result = memcpy(target, RingBuffer_starts_at(buffer), len_tail);
  check(result != NULL, "Failed to write buffer into data.");

  if(len_front) {
	result = memcpy(target + len_tail, buffer->buffer, len_front);
  }
  check(result != NULL, "Failed to write buffer into data.");

  RingBuffer_commit_read(buffer, amount);

  if(buffer->end == buffer->start) {
	buffer->start=0;
   	buffer->end = 0;
  }

  return amount;
error:
  return -1;
}

bstring RingBuffer_gets(RingBuffer *buffer, int amount)
{
  check(amount > 0, "Need more than 0 for gets, you gave: %d", amount);
  check_debug(amount <= RingBuffer_available_data(buffer),
	          "Not enough in the buffer");

  bstring result = blk2bstr(RingBuffer_starts_at(buffer), amount);
  check(result != NULL, "Failed to create gets result.");
  check(blength(result) == amount, "Wrong result length.");

  RingBuffer_commit_read(buffer, amount);
  assert(RingBuffer_available_data(buffer) >= 0 && "Error in read commit.");

  return result;
error:
  return NULL;
}
