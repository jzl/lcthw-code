#ifndef _lcthw_stack_h
#define _lcthw_stack_h

#include <lcthw/list.h>

typedef List Stack;

#define Stack_create List_create
#define Stack_destroy List_destroy
#define Stack_count List_count
#define Stack_peek List_last
#define Stack_push  List_push
#define Stack_pop List_pop
#define STACK_FOREACH(stack, cur) LIST_FOREACH(stack, last, prev, cur)

#endif
