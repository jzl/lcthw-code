#ifndef lcthw_list_h
#define lcthw_list_h

#include <stdlib.h>

struct ListNode;

typedef struct ListNode {
  struct ListNode *next;
  struct ListNode *prev;
  void *value;
} ListNode;

typedef struct List {
  int count;
  ListNode *first;
  ListNode *last;
} List;

List *List_create();
void List_destroy(List *list);
void List_clear(List *list);
void List_clear_destroy(List *list);

#define List_count(A) ((A)->count)
#define List_first(A) ((A)->first != NULL ? (A)->first->value : NULL)
#define List_last(A)  ((A)->last  != NULL ? (A)->last->value : NULL)
#define List_emptyp(A) ((A)->count == 0)

void List_push(List *list, void *value);
void *List_pop(List *list);

void List_insert_after(List *list, ListNode *node, ListNode *new_);
void List_insert_before(List *list, ListNode *node, ListNode *new_);

void List_unshift(List *list, void *value);
void *List_shift(List *list);

void *List_remove(List *list, ListNode *node);

/* operations on the whole list */
List *List_copy(List *source);
List *List_join(List *list1, List *list2);
int List_split(List *list, int n, List *left, List *right);

#define LIST_FOREACH(L, S, M, V) ListNode *_node = NULL;\
							    ListNode *V = NULL;\
								for(V = _node = L->S;_node && _node != L->last->next; V = _node = _node->M)


#endif
									
