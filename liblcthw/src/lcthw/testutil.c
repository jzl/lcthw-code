#include "testutil.h"
#include <time.h>

char *random_str(size_t size)
{
  char *str = malloc(sizeof(char) * (size + 1));
  size_t i;
  for(i = 0; i < size; i++) {
	str[i] = 'a' + random() % 26;
  }
  str[size] = '\0';
  return str;
}
