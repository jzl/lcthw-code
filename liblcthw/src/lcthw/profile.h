static double __average__ = 0.0;
#ifndef _profile_h_
#define _profile_h_
#include <time.h>

#define ___time_lim___ 0.5
#define LAST_AVERAGE() __average__


#define TIMEIT(expr) do{\
  long ___count___;\
  float __time_used__;\
  clock_t ___start___;\
  ___count___ = 0;\
  __time_used__ = 0.0;\
  while(__time_used__ < ___time_lim___) {\
  ___start___ = clock();\
	expr;\
	__time_used__ += (float)(clock()-___start___)/CLOCKS_PER_SEC;\
	___count___++;};\
  __average__ = __time_used__ / ___count___;\
  printf("%s:\n",#expr);\
  printf("\t%ld loops total time:%f seconds %e seconds each\n",___count___,__time_used__,__average__);\
}while(0)
#endif
