#include <lcthw/darray_algos.h>
#include <lcthw/heap.h>
#include <stdio.h>
#include <bsd/stdlib.h>
#include <stdlib.h>
#ifdef _MY_SORT_IMPLEMENTION_
#define qsort(...) _qsort(__VA_ARGS__)
#define heapsort(...) _heapsort(__VA_ARGS__)
#define mergesort(...) _mergesort(__VA_ARGS__)
#endif


static int myQSORT(void *base, int left, int right, size_t size,
	         int(*cmp)(const void *,const void *))
{
  if(right - left <= 0) return 0;
  int i,j;
  i = left;
  j = right;
  void *key = malloc(size);
  check_mem(key);
  memcpy(key, base+left*size, size);
  while(i < j)
  {
	while(i < j && cmp(base+j*size,key) > 0) j--;
	if(i < j) {
	  memcpy(base+i*size, base+j*size, size);
	  i++;}
	while(i < j && cmp(base+i*size,key) < 0) i++;
	if(i < j) {
	  memcpy(base+j*size, base+i*size, size);
	  j--;}
  }
  memcpy(base+i*size,key,size);
  myQSORT(base,left,i-1,size,cmp);
  myQSORT(base,i+1,right,size,cmp);

  return 0;
error:
  return -1;
}

int _qsort(void *base, size_t nmemb, size_t size,
			 int(*cmp)(const void*, const void*))
{
  return myQSORT(base,0,nmemb-1,size,cmp);
}

int _heapsort(void *base, size_t nmemb, size_t size,
			 int(*cmp)(const void*, const void*))
{
  HeapSort(base, size, nmemb, cmp);
  return 0;
}

void _merge(void *base, size_t first, size_t mid, size_t last, size_t size,
			void *temp, int(*cmp)(const void *, const void *))
{
  unsigned int i , j, k;
  i = first, j = mid + 1, k = 0;
  while(i <= mid && j <= last) {
	if(cmp(base+i*size,base+j*size) <= 0) {
	  memcpy(temp+k*size,base+i*size,size);
	  i++;
	  k++;
	} else {
	  memcpy(temp+k*size,base+j*size,size);
	  j++;
	  k++;
	}
  }

  while(i <= mid) {
	memcpy(temp+k*size,base+i*size,size);
	i++;
	k++;
  }

  while(j <= last) {
	memcpy(temp+k*size,base+j*size,size);
	j++;
	k++;
  }

 memcpy(base+first*size,temp,k*size); 
}

void __MergeSort(void *base, size_t first, size_t last, size_t size,
			void *temp, int(*cmp)(const void *, const void *))
{
  if(last <= first) return;
  int mid = (first + last) / 2;
  __MergeSort(base, first, mid, size, temp, cmp);
  __MergeSort(base, mid+1, last, size, temp, cmp);
  _merge(base, first, mid, last, size, temp, cmp);
  
}

int _mergesort(void *base, size_t nmemb, size_t size,
			  int(*cmp)(const void*, const void*))
{
  void *temp = calloc(size, nmemb);
  check_mem(temp);
  __MergeSort(base, 0 ,nmemb-1, size, temp, cmp);
  free(temp);
  return 0;
error:
  if(temp) free(temp);
  return -1;
}




int DArray_qsort(DArray *array, DArray_compare cmp)
{
  qsort(array->contents, DArray_count(array), sizeof(void *), cmp);
  return 0;
}

int DArray_heapsort(DArray *array, DArray_compare cmp)
{
  return heapsort(array->contents, DArray_count(array), sizeof(void *), cmp);
}

int DArray_mergesort(DArray *array, DArray_compare cmp)
{
  return mergesort(array->contents, DArray_count(array), sizeof(void *), cmp);
}

void * DArray_find(DArray *array, void *val, DArray_compare cmp)
{
  int low = 0;
  int high = DArray_count(array) - 1;
  int middle;
  int cmpv;

  while(low <= high) {
	middle = (low + high) / 2;
	cmpv = cmp(array->contents[middle], val);
	if(cmpv > 0)
	  high = middle - 1;
	else if(cmpv < 0)
	  low = middle + 1;
	else
	  return array->contents[middle];
  }

  return NULL;
}

int DArray_sort_add(DArray *array, void *val, DArray_compare cmp)
{
  DArray_push(array, val);
  DArray_mergesort(array,cmp);

  return DArray_count(array);
}

#undef qsort
