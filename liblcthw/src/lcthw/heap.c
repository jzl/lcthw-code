#include <lcthw/heap.h>
static inline int parent(int i)
{
  return (i - 1) / 2;
}

static inline int left(int i)
{
  return 2*i + 1;
}

static inline int right(int i)
{
  return 2*i+2;
}

void Max_Heapify(void *base, unsigned int i, size_t el_size, size_t heap_size,
				 int(*cmp)(const void *, const void *))
{
  unsigned int l = left(i);
  unsigned int r = right(i);
  unsigned int largest;
  if(l < heap_size && cmp(base+el_size*l,base+el_size*i) > 0)
	largest = l;
  else
	largest = i;
  if(r < heap_size && cmp(base+el_size*r,base+el_size*largest) > 0)
	largest = r;

  if(largest != i) {
	swap(base+el_size*i,base+el_size*largest,el_size);
	Max_Heapify(base, largest, el_size, heap_size, cmp);
  }
}

void Build_Max_Heap(void *base, size_t el_size, size_t heap_size,
					int(*cmp)(const void *, const void *))
{
  int i;
  for(i = (heap_size-1)/2; i >= 0; i--)
	Max_Heapify(base, i, el_size, heap_size, cmp);
}

void HeapSort(void *base, size_t el_size, size_t heap_size, 
			  int(*cmp)(const void *, const void *))
{
  Build_Max_Heap(base, el_size, heap_size, cmp);
  int i;
  for(i = heap_size - 1; i >= 0; i--)
  {
	swap(base, base+el_size*i,el_size);
	Max_Heapify(base, 0, el_size, i, cmp);
  }
}
