#include <lcthw/list.h>
#include <lcthw/dbg.h>

List *List_create()
{
  return calloc(1, sizeof(List));
}

void List_destroy(List *list)
{
  check(list != NULL, "trying to destroy a null list");
  LIST_FOREACH(list, first, next, cur) {
	if(cur->prev) {
	  free(cur->prev);
	}
 }

 free(list->last);
 free(list);

error:
 return;
}


void List_clear(List *list)
{
  check(list != NULL, "trying to clear a null list");
  LIST_FOREACH(list, first, next, cur) {
	free(cur->value);
  }

error:
  return;
}


void List_clear_destroy(List *list)
{
  check(list != NULL, "trying to clear&destroy a null list");
  LIST_FOREACH(list, first, next, cur) {
	free(cur->value);
	if(cur->prev) {
	  free(cur->prev);
	}
  }

  free(list->last);
  free(list);
error:
  return;
}


void List_push(List *list, void *value)
{
  check(list != NULL, "trying to push value on a null list");
  ListNode *node = calloc(1, sizeof(ListNode));
  check_mem(node);

  node->value = value;

  if(list->last == NULL) {
	list->first = node;
	list->last = node;
  } else {
	list->last->next = node;
	node->prev = list->last;
	list->last = node;
  }

  list->count++;
  check(list->count >0 && list->first != NULL, "fail on pushing value.");

error:
  return;
}

void List_insert_after(List *list, ListNode *node, ListNode *new_)
{
  check(node != NULL, "can't insert a node after NULL");
  new_->prev = node;
  new_->next = node->next;
  node->next = new_;
  if(new_->next) {
	new_->next->prev = new_;
  } else {
	list->last = new_;
  }
  list->count += 1;
error:
  return;
}

void List_insert_before(List *list, ListNode *node, ListNode *new_)
{
  check(node != NULL, "can't insert a node before NULL.");
  new_->next = node;
  new_->prev = node->prev;
  node->prev = new_;
  if(new_->prev) {
	new_->prev->next = new_;
  } else {
	list->first = new_;
  }

  list->count += 1;
error:
  return;
}


void *List_pop(List *list)
{
  check(list != NULL, "trying to pop value on a null list");

  ListNode *node = list->last;
  return node != NULL ? List_remove(list, node) : NULL;

error:
  return NULL;
}

void List_unshift(List *list, void *value)
{
  check(list != NULL, "trying to insert value on a null list");

  ListNode *node = calloc(1, sizeof(ListNode));
  check_mem(node);

  node->value = value;

  if(list->first == NULL) {
	list->first = node;
	list->last = node;
  } else {
	node->next = list->first;
	list->first->prev = node;
	list->first = node;
  }

  list->count++;
  check(list->count >0 && list->first != NULL, "fail on inserting value.");

error:
  return;
}

void *List_shift(List *list)
{
  check(list != NULL, "trying to shift a null list");

  ListNode *node = list->first;
  return node != NULL ? List_remove(list, node) : NULL;

error:
  return NULL;
}

void *List_remove(List *list, ListNode *node)
{
  check(list != NULL, "trying to remove node on a null list");

  void *result = NULL;

  check(list->first && list->last, "List is empty.");
  check(node, "node can't be NULL");

  if(node == list->first && node == list->last) {
	list->first = NULL;
	list->last = NULL;
  } else if(node == list->first) {
	list->first = node->next;
	check(list->first != NULL, "Invalid list, somehow got a first that is NULL.");
	list->first->prev = NULL;
  } else if(node == list->last) {
	list->last = node->prev;
	check(list->last != NULL, "Invalid list, somehow got a next that is NULL.");
	list->last->next = NULL;
  } else {
	ListNode *after = node->next;
	ListNode *before = node->prev;
	after->prev = before;
	before->next = after;
  }

  list->count--;
  check(list->count >= 0, "it's impossible for a list have a negative number of nodes.");
  result = node->value;
  free(node);

  return result;

error:
  return result;
}

List *List_copy(List *list)
{
  List *result = List_create();

  LIST_FOREACH(list, first, next, cur) {
	List_push(result, cur->value);
  }

  return result;

}

List *List_join(List *list1, List *list2)
{
  check(list1 && list2, "invalid List pointer");
  List *result = NULL;
  List *list1cpy,*list2cpy;

  check((list1cpy = List_copy(list1)) != NULL, "Failed to copy list1");
  check((list2cpy = List_copy(list2)) != NULL, "Failed to copy list2");

  if(List_emptyp(list1)) {
	result = list2cpy;
  } else if(List_emptyp(list2)) {
	result = list1cpy;
  } else {
	list1cpy->last->next = list2cpy->first;
	list2cpy->first->prev = list1cpy->last;
	list1cpy->count += list2cpy->count;
	result = list1cpy;
  }

  return result;

error:
  return NULL;
}


int List_split(List *list, int n, List *left, List *right)
{

  int i;
  ListNode *cur = NULL;

  if(n <=  0) { 		// n <= 0,left is a blank list,right is equal to list
	right->count = list->count;
	right->first = list->first;
	right->last = list->last;
  } else if(n >= list->count) { 	// n >= list->count, left is equal to list, right is blank
	left->first = list->first;
	left->count = list->count;
	left->last =  list->last;
  } else  {			// 0<n<list->count,i==n
	for(i = 0, cur = list->first; cur && i < n; i++,cur=cur->next); 
	left->first = list->first;
	left->count = n;
	left->last = cur->prev;
	right->first = cur;
	right->count = List_count(list) - n;
	right->last = list->last;
  }


  return 0;
}
