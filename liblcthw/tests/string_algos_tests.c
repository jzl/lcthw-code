#include "minunit.h"
#include <lcthw/testutil.h>
#include <lcthw/profile.h>
#include <lcthw/string_algos.h>
#include <lcthw/bstrlib.h>
#include <time.h>

struct tagbstring IN_STR = bsStatic("I have a ALPHA beta ALPHA and oranges ALPHA");
struct tagbstring ALPHA = bsStatic("ALPHA");
const int TEST_TIME = 1;

char *test_find_and_scan()
{
  StringScanner *scan = StringScanner_create(&IN_STR);
  mu_assert(scan != NULL, "Failed to make the scanner.");

  int find_i = String_find(&IN_STR, &ALPHA);
  mu_assert(find_i > 0, "Failed to find 'ALPHA' in test string");

  int scan_i = StringScanner_scan(scan, &ALPHA);
  mu_assert(scan_i > 0, "Failed to find 'ALPHA' with scan");
  mu_assert(scan_i == find_i, "find and scan don't match");

  scan_i = StringScanner_scan(scan, &ALPHA);
  mu_assert(scan_i > find_i, "should find another ALPHA after the first");

  scan_i = StringScanner_scan(scan, &ALPHA);
  mu_assert(scan_i > find_i, "should find another ALPHA after the first");

  mu_assert(StringScanner_scan(scan, &ALPHA) == -1, "should not find it");

  StringScanner_destroy(scan);

  return NULL;
}

char *test_binstr_performance()
{
  int found_at = 0;

  TIMEIT(
	  found_at = binstr(&IN_STR, 0, &ALPHA);
	  mu_assert(found_at != BSTR_ERR, "Failed to find!");
	  );

  return NULL;
}

char *test_find_performance()
{

  TIMEIT(String_find(&IN_STR, &ALPHA));
  return NULL;
}

char *test_scan_performance()
{
  StringScanner *scan;

  TIMEIT(scan=StringScanner_create(&IN_STR);StringScanner_destroy(scan));
  scan = StringScanner_create(&IN_STR);
  TIMEIT(StringScanner_scan(scan, &ALPHA);scan->hlen=0;);

  StringScanner_destroy(scan);

  return NULL;
}

char *test_performance()
{
  char *cfront, *ctail, *cnoise;
  bstring front, tail, whole, noise;
  StringScanner *scan;

  cfront = random_str(100000000);
  ctail = random_str(50000);
  cnoise = random_str(1000);

  // set up the strings
  front = bfromcstr(cfront);
  tail = bfromcstr(ctail);
  noise = bfromcstr(cnoise);
  whole = bstrcpy(noise);
  bconcat(whole, front);
  bconcat(whole, noise);
  bconcat(whole, tail);

  free(cfront);
  free(ctail);
  free(cnoise);


  TIMEIT(assert(String_find(whole, front) >=0));
  TIMEIT(assert(String_find(whole, tail) >=0));
  TIMEIT(assert(binstr(whole, 0 ,front) >=0));
  TIMEIT(assert(binstr(whole, 0 ,tail) >=0));

  TIMEIT(scan = StringScanner_create(whole);StringScanner_scan(scan,front); StringScanner_destroy(scan));


  bdestroy(front);
  bdestroy(tail);
  bdestroy(noise);
  bdestroy(whole);
  return NULL;
}


char *all_tests()
{
  mu_suite_start();

  mu_run_test(test_find_and_scan);
  mu_run_test(test_performance);

  // this is an idiom for commenting out sections of code
#if 1
  mu_run_test(test_scan_performance);
  mu_run_test(test_find_performance);
  mu_run_test(test_binstr_performance);
#endif

  return NULL;
}

RUN_TESTS(all_tests);
