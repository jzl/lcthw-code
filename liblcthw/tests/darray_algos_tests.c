#include "minunit.h"
#include <lcthw/testutil.h>
#include <lcthw/profile.h>
#include <lcthw/darray_algos.h>


int testcmp(char **a, char **b)
{
  return strcmp(* a, * b);
}

DArray *create_words(int size, int num)
{
  DArray *result = DArray_create(size+1, num);
  int i = 0;

  for(i = 0; i < num; i++) {
	DArray_push(result, random_str(size));
  }

  return result;
}

int is_sorted(DArray *array)
{
  int i = 0;
  for(i = 0; i < DArray_count(array) - 1; i++) {
	if(strcmp(DArray_get(array, i), DArray_get(array, i+1)) > 0) {
	  return 0;
	}
  }

  return 1;
}


char *run_sort_test(int (*func)(DArray *, DArray_compare), const char *name)
{
  DArray *words = create_words(4,20);
  mu_assert(!is_sorted(words), "Words should start not sorted.");

  debug("--- Testing %s sorting algorithm", name);
  int rc = func(words, (DArray_compare)testcmp);
  mu_assert(rc == 0, "sort failed.");
  mu_assert(is_sorted(words), "didn't sort it.");

  DArray_destroy(words);

  return NULL;
}

char *sort_performance_test()
{
  int i;
  for(i = 10; i < 1E6; i*=10) {
	DArray *words = create_words(10, i);
#ifdef sort_first
	DArray_qsort(words, (DArray_compare)testcmp);
#endif
	DArray *wordscpy = DArray_deepcopy(words);
	printf("i=%d\n",i);
	TIMEIT(DArray_qsort(wordscpy,(DArray_compare)testcmp);mu_assert(is_sorted(wordscpy),"should be sorted.");DArray_clear_destroy(wordscpy);wordscpy=DArray_deepcopy(words));
	TIMEIT(DArray_heapsort(wordscpy,(DArray_compare)testcmp);mu_assert(is_sorted(wordscpy),"should be sorted.");DArray_clear_destroy(wordscpy);wordscpy=DArray_deepcopy(words));
	TIMEIT(DArray_mergesort(wordscpy,(DArray_compare)testcmp);mu_assert(is_sorted(wordscpy),"should be sorted.");DArray_clear_destroy(wordscpy);wordscpy=DArray_deepcopy(words));
  }

  return NULL;
}

char *test_qsort()
{
  return run_sort_test(DArray_qsort, "qsort");
}

char *test_heapsort()
{
  return run_sort_test(DArray_heapsort, "heapsort");
}

char *test_mergesort()
{
  return run_sort_test(DArray_mergesort, "mergesort");
}

char *all_tests()
{
  mu_suite_start();
//  mu_run_test(test_qsort);
//  mu_run_test(test_heapsort);
//  mu_run_test(test_mergesort);
	mu_run_test(sort_performance_test);

  return NULL;
}

RUN_TESTS(all_tests);
