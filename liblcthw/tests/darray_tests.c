#include "minunit.h"
#include <lcthw/darray.h>
#include <lcthw/profile.h>

static DArray *array = NULL;
static int *val1 = NULL;
static int *val2 = NULL;

char *test_create()
{
  array = DArray_create(sizeof(int), 100);
  mu_assert(array != NULL, "DArray_create failed.");
  mu_assert(array->contents != NULL, "contents are wrong in darray");
  mu_assert(array->end == 0, "end isn't at the right spot");
  mu_assert(array->element_size == sizeof(int), "element_size is wrong.");
  mu_assert(array->max == 100, "wrong max length on initial size");

  return NULL;
}

char *test_destroy()
{
  DArray_destroy(array);

  return NULL;
}

char *test_new()
{
  val1 = DArray_new(array);
  mu_assert(val1 != NULL, "failed to make a new element");

  val2 = DArray_new(array);
  mu_assert(val2 != NULL, "failed to make a new element");

  return NULL;
}

char *test_set()
{
  DArray_set(array, 0 , val1);
  DArray_set(array, 1 , val2);

  return NULL;
}

char *test_get()
{
  mu_assert(DArray_get(array, 0) == val1, "Wrong first value.");
  mu_assert(DArray_get(array, 1) == val2, "Wrong second value.");

  return NULL;
}

char *test_remove()
{
  int *val_check = DArray_remove(array, 0);
  mu_assert(val_check != NULL, "Should not get NULL.");
  mu_assert(*val_check == *val1, "Should get the first value.");
  mu_assert(DArray_get(array, 0) == NULL, "Should be gone.");
  DArray_free(val_check);

  val_check = DArray_remove(array, 1);
  mu_assert(val_check != NULL, "Should not get NULL.");
  mu_assert(*val_check == *val2, "Should get the second value.");
  mu_assert(DArray_get(array, 1) == NULL, "Should be gone.");
  DArray_free(val_check);

  return NULL;
}

char *test_expand_contract()
{
  int old_max = array->max;
  DArray_expand(array);
  mu_assert((unsigned int)array->max == old_max + array->expand_rate,
	  		"Wrong size after expand.");

  DArray_contract(array);

  mu_assert((unsigned int)array->max == array->expand_rate + 1,
	  		"Should stay at the expand_rate at least.");

  DArray_contract(array);

  mu_assert((unsigned int)array->max == array->expand_rate + 1,
	  		"Should stay at the expand_rate at least.");

  return NULL;
}

char *test_push_pop()
{
  int i = 0;
  for(i = 0; i < 1000; i++) {
	int *val = DArray_new(array);
	*val = i * 333;
	DArray_push(array, val);
  }

  mu_assert(array->max == 1201, "Wrong max size.");

  for(i = 999; i >= 0; i--) {
	int *val = DArray_pop(array);
	mu_assert(val != NULL, "Shouldn't get a NULL.");
	mu_assert(*val == i * 333, "Wrong value.");
	DArray_free(val);
  }

  return NULL;
}

char *test_clear()
{
  int i;
  array = DArray_create(sizeof(int), 100);
  for(i = 0; i < 50; i++)
  {
	int *val = DArray_new(array);
	*val = i * 333;
	DArray_push(array, val);
  }
  mu_assert(array->max == 100, "array's max should be 100");
  DArray_clear(array);
  for(i = 0;i < DArray_max(array); i++) {
	mu_assert(array->contents[i] == NULL, "value should be NULL after clear.");
  }
  mu_assert(DArray_count(array) == 0, "array's count should be 0 after clearing.");

  return NULL;
}

/* test operations' performance */
char *test_performance()
{
  DArray *array = DArray_create(sizeof(int), 6E6);
  int *val = DArray_new(array);
  int *val2 = DArray_new(array);
  *val = 999;
  *val2 = 111;
  TIMEIT(DArray_push(array, val));
  TIMEIT(DArray_push(array, val));
  TIMEIT(DArray_pop(array));
  TIMEIT(DArray_get(array, 97));
  TIMEIT(DArray_set(array, 97, val2));
  TIMEIT(DArray_contract(array));
  TIMEIT(DArray_expand(array));
  TIMEIT(free(DArray_new(array)));

  TIMEIT(DArray_destroy(DArray_create(sizeof(int), 1E1)));
  TIMEIT(DArray_destroy(DArray_create(sizeof(int), 1E2)));
  TIMEIT(DArray_destroy(DArray_create(sizeof(int), 1E3)));
  TIMEIT(DArray_destroy(DArray_create(sizeof(int), 1E4)));
  TIMEIT(DArray_destroy(DArray_create(sizeof(int), 1E5)));
  TIMEIT(DArray_destroy(DArray_create(sizeof(int), 1E6)));
  TIMEIT(DArray_destroy(DArray_create(sizeof(int), 1E7)));
  TIMEIT(DArray_destroy(DArray_create(sizeof(int), 1E8)));

  DArray_destroy(array);
  free(val);
  free(val2);

  return NULL;
}

char *all_tests() {
  mu_suite_start();

  mu_run_test(test_create);
  mu_run_test(test_new);
  mu_run_test(test_set);
  mu_run_test(test_get);
  mu_run_test(test_remove);
  mu_run_test(test_expand_contract);
  mu_run_test(test_push_pop);
  mu_run_test(test_destroy);
  mu_run_test(test_clear);
  mu_run_test(test_performance);

  return NULL;
}

RUN_TESTS(all_tests);

