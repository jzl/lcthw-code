#include "minunit.h"
#include <lcthw/ringbuffer.h>
#include <lcthw/testutil.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

static RingBuffer *rb = NULL;
const int buffer_len = 512;

char *test_create()
{
  rb = RingBuffer_create(buffer_len);

  mu_assert(rb != NULL, "failed to create buffer");
  mu_assert(RingBuffer_available_space(rb) == buffer_len, "length wrong");
  mu_assert(RingBuffer_available_data(rb) == 0, "should have no data");

  return NULL;
}

char *test_read_write()
{
  int i;
  int write_len;
  int read_len;
  char *src,*dst;

  srandom(time(NULL));
  for(i = 0; i < 1000; i++)
  {
//	if(RingBuffer_full(rb)) continue;
	write_len = random() % RingBuffer_available_space(rb);
	src = random_str(write_len);

	mu_assert(RingBuffer_write(rb, src, write_len) == write_len, "write failed");

//	if(RingBuffer_empty(rb)) continue;
	read_len = random() % (RingBuffer_available_data(rb));
	dst = calloc(read_len, sizeof(char));
	mu_assert(RingBuffer_read(rb, dst, read_len) == read_len, "read failed");

	free(src);
	free(dst);
  }

  return NULL;
}

char *test_detroy()
{
  RingBuffer_destroy(rb);

  return NULL;
}

char *all_tests()
{
  mu_suite_start();

  mu_run_test(test_create);
  mu_run_test(test_read_write);
  mu_run_test(test_detroy);

  return NULL;
}

RUN_TESTS(all_tests);
