#include <lcthw/bstrlib.h>
#include <lcthw/testutil.h>
#include <stdio.h>
#include "minunit.h"



char *string_test()
{
  char *cstr1;
  bstring str1, str2;
  
  cstr1 = random_str(10);
  str1 = bfromcstr(cstr1);
  mu_assert(blength(str1) == 10, "wrong bstring length.");
  mu_assert(biseqcstr(str1, cstr1), "bstring should be equal to its original cstring.");

  str2 = bstrcpy(str1);
  mu_assert(biseq(str1, str2), "bscpy failed.");

  bconcat(str1, str2);
  mu_assert(blength(str1) == 2*blength(str2), "length wrond,bconcat failed");
  mu_assert(binstr(str1,0,str2) != BSTR_ERR, "str2 should be in str1, concat failed.");

  bdelete(str1,0,blength(str2));
  mu_assert(biseq(str1,str2),"bdelete failed");

  bdestroy(str1);
  bdestroy(str2);
  free(cstr1);

  return NULL;
}

char *string_list_test()
{
  char *testfilename = "bstrtest.txt";
  char *cstr;
  FILE *testfile;
  bstring str;
  struct bstrList *strList;
  struct bStream *bstream;
  int i = 0;

  cstr = random_str(100);
	
  testfile = fopen(testfilename,"r");
  mu_assert(testfile, "open file faild");

  bstream = bsopen((bNread)fread, testfile);
  mu_assert(bstream, "bsopen failed.");

  strList = bstrListCreate();
  mu_assert(strList, "create bstrlist failed.");
  mu_assert(bstrListAlloc(strList, 20) == BSTR_OK, "listalloc failed");;
  mu_assert(strList->mlen >= 20, "strlistalloc failed.");

  while(!bseof(bstream)) {
	str = bfromcstr(cstr);
	balloc(str, 128);
	bsreadln(str, bstream, '\n');
	strList->entry[i] = str;
	i++;
	strList->qty++;
  }

  str = bjoin(strList,bfromcstr("\n"));
  printf("%s\n",str->data);

  free(cstr);
  bdestroy(str);
  bstrListDestroy(strList);

  bsclose(bstream);

  return NULL;
}

char *all_tests()
{
  mu_suite_start();

  mu_run_test(string_test);
  mu_run_test(string_list_test);

  return NULL;
}

RUN_TESTS(all_tests);
