#include "minunit.h"
#include <string.h>
#include <lcthw/list.h>
#include <assert.h>

static List *list = NULL;
char *test1 = NULL;
char *test2 = NULL;
char *test3 = NULL;

void init_test_data()
{
  test1 = malloc(18*sizeof(char));
  test2 = malloc(18*sizeof(char));
  test3 = malloc(18*sizeof(char));
  
  strcpy(test1, "test1 data");
  strcpy(test2, "test2 data");
  strcpy(test3, "test3 data");
}

char *test_create()
{
  list = List_create();
  mu_assert(list != NULL, "Failed to create list.");

  return NULL;
}


char *test_destroy()
{
  List_clear_destroy(list);

  return NULL;

}


char *test_push_pop()
{
  List_push(list, test1);
  mu_assert(List_last(list) == test1, "Wrong last value.");

  List_push(list, test2);
  mu_assert(List_last(list) == test2, "Wrong last value.");

  List_push(list, test3);
  mu_assert(List_last(list) == test3, "Wrong last value.");
  mu_assert(List_count(list) == 3, "Wrong count on push.");

  char *val = List_pop(list);
  mu_assert(val == test3, "Wrong value on pop.");

  val = List_pop(list);
  mu_assert(val == test2, "Wrong value on pop.");

  val = List_pop(list);
  mu_assert(val == test1, "Wrong value on pop.");
  mu_assert(List_count(list) == 0, "Wrong count after pop.");

  return NULL;
}

char *test_unshift()
{
  List_unshift(list,test1);
  mu_assert(List_first(list) == test1, "Wrong first value.");

  List_unshift(list,test2);
  mu_assert(List_first(list) == test2, "Wrong first value.");

  List_unshift(list,test3);
  mu_assert(List_first(list) == test3, "Wrong first value.");
  mu_assert(List_count(list) == 3, "Wrong count on unshift.");

  return NULL;
}

char *test_remove()
{
  // we only need to test the middle remove case since push/shift
  // already tests the other cases

  char *val = List_remove(list, list->first->next);
  mu_assert(val == test2, "Wrong removed element.");
  mu_assert(List_count(list) == 2, "Wrong count after remove.");
  mu_assert(List_first(list) == test3, "Wrong first after remove.");
  mu_assert(List_last(list) == test1, "Wrong last after remove.");

  return NULL;
}


char *test_shift()
{
  mu_assert(List_count(list) != 0, "Wrong count before shift.");

  char *val = List_shift(list);
  mu_assert(val == test3, "Wrong value on shift.");

  val = List_shift(list);
  mu_assert(val == test1, "Wrong value on shift.");
  mu_assert(List_count(list) == 0, "Wrong count after shift.");

  return NULL;
}

char *test_join()
{
  List *list1,*list2,*result;
  list1 = List_copy(list);
  list2 = List_copy(list);
  result = List_join(list1, list2);

  mu_assert((List_count(list1)+List_count(list2))==List_count(result),
	  		"joined list's count should equal the sum of child lists' counts");


  List_destroy(result);
  List_destroy(list1);
  List_destroy(list2);
  return NULL;

  
}

void print_list(List *list)
{
  if(!list) return;
  LIST_FOREACH(list, first, next, cur) {
	if(cur->value)
	  printf("\t%s\n",(char *)cur->value);
  }
}


char *all_tests() {
  mu_suite_start();

  init_test_data();

  mu_run_test(test_create);
  
  mu_run_test(test_push_pop);

  mu_run_test(test_unshift);

  mu_run_test(test_join);

  mu_run_test(test_remove);

  mu_run_test(test_shift);


  mu_run_test(test_destroy);
  
  return NULL;
}

RUN_TESTS(all_tests);
