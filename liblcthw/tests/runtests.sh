echo "Running unit tests:"
files=$(ls -lrt tests/*_tests|tail -1|awk '{print $9}')

for i in $files
do
  if test -f $i
  then
	if $VALGRIND ./$i 2>> tests/tests.log
	then
	  echo $i PASS
	else
	  echo "ERROR in test $i: here's tests/tests.log"
	  echo "-----------"
	  tail tests/tests.log
	  exit 1
	fi
  fi
done

echo ""
