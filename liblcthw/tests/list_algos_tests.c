#include "minunit.h"
#include <lcthw/testutil.h>
#include <lcthw/list_algos.h>
#include <lcthw/profile.h>
#include <assert.h>
#include <string.h>
#include <stdlib.h>

char *values[] = {"XXXX", "1234", "abcd", "xjvef", "NDSS","I love you","what the hell"};
#define NUM_VALUES 1000

void print_list(List *list)
{
  if(!list) return;
  LIST_FOREACH(list, first, next, cur) {
	if(cur->value)
	  printf("\t%s\n",(char *)cur->value);
  }
  printf("\n\n");
}


List *create_words(int len)
{
  int i = 0;
  List *words = List_create();

  for(i=0;i < len; i++) {
	List_push(words, random_str(5));
  }

  return words;
}

int is_sorted(List *words)
{
  LIST_FOREACH(words, first, next, cur) {
	if(cur->next && strcmp(cur->value, cur->next->value) > 0) {
	  debug("%s %s",(char *)cur->value, (char *)cur->next->value);
	  return 0;
	}
  }

  return 1;
}

char *test_bubble_sort()
{
  List *words = create_words(NUM_VALUES);

  // should work on a list that needs sorting
  int rc = List_bubble_sort(words, (List_compare)strcmp);
  mu_assert(rc == 0, "Bubble sort failed.");
  mu_assert(is_sorted(words), "Words are not sorted after bubble sort."); // should work on already sorted list
  rc = List_bubble_sort(words, (List_compare)strcmp);
  mu_assert(rc == 0, "Bubble sort failed.");
  mu_assert(is_sorted(words), "Words should be sorted if already bubble sorted");

  List_destroy(words);

 // should work on an empty list
  words = List_create();
  rc = List_bubble_sort(words, (List_compare)strcmp);
  mu_assert(rc == 0, "Bubble sort failed on empty list.");
  mu_assert(is_sorted(words), "Words should be sorted if empty");

  List_destroy(words);

  return NULL;
  
}

char *test_merge_sort()
{
  List *words = create_words(NUM_VALUES);
  // should workd on a list that needs sorting
  List *res = List_merge_sort(words, (List_compare)strcmp);
  mu_assert(is_sorted(res), "Words are not sorted after merge sort.");


  List *res2 = List_merge_sort(res, (List_compare)strcmp);
  mu_assert(is_sorted(res), "Should still be sorted after merge sort.");
  List_destroy(res2);
  List_destroy(res);

  List_destroy(words);
  return NULL;
}

char *test_insert_sorted()
{
  int i;
  int L;
  char *data[] = {"abcde","this is me","hello there"};
  List *words = create_words(NUM_VALUES);
  L = List_count(words);

  mu_assert(List_bubble_sort(words, (List_compare)strcmp) == 0, "bubble sort failed.");
  mu_assert(is_sorted(words), "bubble sort didn't work properly.");

  for(i = 1; i < 3; i++) {
	List_insert_sorted(words, data[i], (List_compare)strcmp);
	mu_assert(List_count(words) == L+i, "wrong list count after insertion.");
	mu_assert(is_sorted(words), "insert in the wrong place.");
  }
	
  return NULL;
}

char *compare_bubble_merge()
{
  double average;
  long len;
  for(len = 10;len <= 1E5; len *= 10)
  {
	List *words = create_words(len);
	TIMEIT(List_merge_sort(words, (List_compare)strcmp));
	average = LAST_AVERAGE();
	mu_assert(!is_sorted(words), "merge sort don't change the original list");
	TIMEIT(List_bubble_sort(words, (List_compare)strcmp));

	printf("for list length = %ld:\n",len);
	if(average > LAST_AVERAGE())
	  printf("bubble sort won.\n");
	else
	  printf("merge sort won.\n");
  }

  return NULL;

}

char *all_tests()
{
  mu_suite_start();

  mu_run_test(test_bubble_sort);
  mu_run_test(test_merge_sort);
  mu_run_test(test_insert_sorted);
  mu_run_test(compare_bubble_merge);

  return NULL;
}

RUN_TESTS(all_tests);

