#include "minunit.h"
#include <lcthw/hashmap.c>
#include <lcthw/testutil.h>
#include <lcthw/profile.h>
#include <assert.h>
#include <lcthw/bstrlib.h>
#define BUFFER_LEN 20
#define NUM_KEYS 10000

Hashmap *map = NULL;
static int traverse_called = 0;
struct tagbstring test1 = bsStatic("test data 1");
struct tagbstring test2 = bsStatic("test data 2");
struct tagbstring test3 = bsStatic("test data 3");
struct tagbstring expect1 = bsStatic("THE VALUE 1");
struct tagbstring expect2 = bsStatic("THE VALUE 2");
struct tagbstring expect3 = bsStatic("THE VALUE 3");

static int traverse_good_cb(HashmapNode *node)
{
  debug("KEY: %s", bdata((bstring)node->key));
  traverse_called++;
  return 0;
}


bstring gen_keys()
{
  char *ckey = random_str(20);
  check_mem(ckey);
  bstring key = bfromcstr(ckey);

  free(ckey);
  return key;
error:
  if(ckey) free(ckey);
  return NULL;
}


static int traverse_fail_cb(HashmapNode *node)
{
  debug("KEY: %s", bdata((bstring)node->key));
  traverse_called++;

  if(traverse_called == 2) {
	return 1;
  } else {
	return 0;
  }
}


char *test_create()
{
  map = Hashmap_create(NULL, NULL, -1);
  mu_assert(map != NULL, "Failed to create map.");

  return NULL;
}

char *test_destroy()
{
  Hashmap_destroy(map);

  return NULL;
}


char *test_get_set()
{
  int rc = Hashmap_set(map, &test1, &expect1);
  mu_assert(rc == 0, "Failed to set &test1");
  bstring result = Hashmap_get(map, &test1);
  mu_assert(result == &expect1, "Wrong value for test1.");

  rc = Hashmap_set(map, &test2, &expect2);
  mu_assert(rc == 0, "Failed to set &test2");
  result = Hashmap_get(map, &test2);
  mu_assert(result == &expect2, "Wrong value for test2.");

  rc = Hashmap_set(map, &test3, &expect3);
  mu_assert(rc == 0, "Failed to set &test3");
  result = Hashmap_get(map, &test3);
  mu_assert(result == &expect3, "Wrong value for test3.");

  return NULL;
}

char *test_traverse()
{
  int rc = Hashmap_traverse(map, traverse_good_cb);
  mu_assert(rc == 0, "Failed to traverse");
  mu_assert(traverse_called == 3, "Wrong count traverse");

  traverse_called = 0;
  rc = Hashmap_traverse(map, traverse_fail_cb);
  mu_assert(rc == 1, "Failed to traverse");
  mu_assert(traverse_called == 2, "Wrong count traverse for fail");

  return NULL;
}

char *test_delete()
{
  bstring deleted = (bstring)Hashmap_delete(map, &test1);
  mu_assert(deleted != NULL , "Got NULL on delete.");
  mu_assert(deleted == &expect1, "Should get test1");
  bstring result = Hashmap_get(map, &test1);
  mu_assert(result == NULL, "Should deleted.");

  deleted = (bstring)Hashmap_delete(map, &test2);
  mu_assert(deleted != NULL , "Got NULL on delete.");
  mu_assert(deleted == &expect2, "Should get test2");
  result = Hashmap_get(map, &test2);
  mu_assert(result == NULL, "Should deleted.");

  deleted = (bstring)Hashmap_delete(map, &test3);
  mu_assert(deleted != NULL , "Got NULL on delete.");
  mu_assert(deleted == &expect3, "Should get test3");
  result = Hashmap_get(map, &test3);
  mu_assert(result == NULL, "Should deleted.");

  return NULL;
}

char *test_performance()
{
  Hashmap *map1 = Hashmap_create(NULL, NULL, 5);
  Hashmap *map2 = Hashmap_create(NULL, NULL, 7E5);
  DArray *keys = DArray_create(sizeof(bstring), 1000);
  DArray *values = DArray_create(sizeof(char *), 1000);

  char *value;
  bstring key;
  int i;
  int key_num1, key_num2;

  for(i = 0; i <= 1E6; i ++) {
	key = gen_keys();
	DArray_push(keys, key);
	value = random_str(10);
	DArray_push(values, value);
  }	

  i = 0;

  TIMEIT(
	  	 mu_assert(Hashmap_set(map1,DArray_get(keys,i), DArray_get(values, i)) == 0,
		  		   "fail to set");
		 i++;
		);

  key_num1 = i;
  i = 0;

  TIMEIT(
	  	 mu_assert(Hashmap_set(map2,DArray_get(keys,i), DArray_get(values, i)) == 0,
		  		   "fail to set");
		 i++;
		 );

  key_num2 = i;

  printf("map1's occupy tatio:%f\n", Hashmap_occupy_ratio(map1));
  printf("map2's occupy tatio:%f\n", Hashmap_occupy_ratio(map2));

TIMEIT(i = random() % key_num1; mu_assert(Hashmap_get(map1, DArray_get(keys, i)) == DArray_get(values, i),
		           "key/value does not match"));

  TIMEIT(i = random() % key_num2;
	  	 mu_assert(Hashmap_get(map2, DArray_get(keys, i)) == DArray_get(values, i),
		           "key/value does not match"));


  Hashmap_destroy(map1);
  Hashmap_destroy(map2);
  for(i = 0; i < DArray_count(keys); i++)
  {
	bdestroy(DArray_get(keys, i));
	free(DArray_get(values, i));
  }
  DArray_clear_destroy(keys);
  DArray_clear_destroy(values);

  return NULL;
}

char *all_tests()
{
  mu_suite_start();

//  mu_run_test(test_create);
//  mu_run_test(test_get_set);
//  mu_run_test(test_traverse);
//  mu_run_test(test_delete);
//  mu_run_test(test_destroy);
  mu_run_test(test_performance);

  return NULL;
}

RUN_TESTS(all_tests);

