#include <stdio.h>

int main(int argc, char *argv[])
{
  int i = 0;
  //go through each string in argv
  //why am I skipping argv[0]

  //try assign value to argv's element
  /*argv[1] = "hahaha";*/

  for(i = 1; i < argc; i++) {
	printf("arg %d: %s \n", i, argv[i]);
  }

  //let's make our own array of strings
  char *states[] = {
	"California", "Oregon",
	"Washington", "Texas"
  };
  //try assign an element from the argv array to states array
  /*states[1] = argv[1];*/

  //try assign an element from the states array to argv array
  /*for(i=0;i<4;i++) argv[i] = states[i];*/
  /*for(i = 1; i < argc; i++) {*/
	/*printf("arg %d: %s \n", i, argv[i]);*/
  /*}*/

  int num_states = 4;

  for(i = 0; i < num_states; i++) {
	printf("state %d: %s\n",i,states[i]);
  }

  return 0;
}
