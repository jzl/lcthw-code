CFLAGS=-Wall -g
OBJS=ex1 ex3 ex4 ex5 ex6 ex7 ex8 ex9 ex10 ex11 ex12 ex13 ex14 ex15 ex16
OBJS+= ex16e ex17 ex18  ex20 ex21 ex22 ex23 ex24 ex25 ex27

clean:
	rm -rf $(OBJS)

all: $(OBJS)

ex22:  ex22_main.c ex22.o

ex22.o: ex22.c ex22.h
