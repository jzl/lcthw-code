cd tests

# try it out with some things that work
ex29_tests ../build/libex29.so print_a_message "hello there"

./ex29_tests ../build/libex29.so print_a_message "hello there"

./ex29_tests ../build/libex29.so uppercase "hello there"

./ex29_tests ../build/libex29.so lowercase "HELLO tHeRe"

./ex29_tests ../build/libex29.so fail_on_purpose "i fail"

# try give it bad args
./ex29_tests ../build/libex29.so asderfsea asdfresa

./ex29_tests ../build/libex.so asderfsea asdfresa


