/*
 * =====================================================================================
 *
 *       Filename:  ex21.c
 *
 *    Description:  Advanced data types and flow control
 *
 *        Version:  1.0
 *        Created:  12/11/2012 04:30:15 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Ji.Zhilong 
 *   Organization:  
 *
 * =====================================================================================
 */
#include <stdio.h>

void print_type_size()
{
  printf("sizeof(char) = %ld\n",sizeof(char));
  printf("sizeof(int) = %ld\n",sizeof(int));
  printf("sizeof(float) = %ld\n",sizeof(float));
  printf("sizeof(double) = %ld\n",sizeof(double));
  printf("sizeof(long int) = %ld\n",sizeof(long int));
  //There is no type of long float
  //printf("sizeof(long float) = %ld\n",sizeof(long float));
  printf("sizeof(long double) = %ld\n",sizeof(long double));
  printf("sizeof(size_t) = %ld\n",sizeof(size_t));
  printf("sizeof(void) = %ld\n",sizeof(void));
  printf("sizeof(void *) = %ld\n",sizeof(void *));
  printf("sizeof(long *) = %ld\n",sizeof(long *));
}


int main(int argc, char *argv[])
{
  print_type_size();
  return 0;
}
