#ifndef _commands_h
#define _commands_h

#include <apr_pools.h>

#define DEPENDS_PATH "/tmp/DEPENDS"
#define TAR_GZ_SRC "/tmp/pkg-src.tar.gz"
#define TAR_BZ2_SRC "/tmp/pkg-src.tar.bz2"
#define BUILD_DIR "/tmp/pkg-build"
#define GIT_PAT "*.git"
#define DEPEND_PAT "*DEPENDS"
#define TAR_GZ_PAT "*.tar.gz"
#define TAR_BZ2_PAT "*.tar.bz2"
#define CONFIG_SCRIPT "/tmp/pkg-build/configure"

enum CommandType {
  COMMAND_DONE, COMMAND_INSTALL, COMMAND_LIST, COMMAND_FETCH,
  COMMAND_INIT, COMMAND_BUILD, COMMAND_RESET
};

typedef enum CommandType CommandType;

enum URLTYPE {
  URL_GIT,URL_DEPEND, URL_TAR_GZ, URL_TAR_BZ2, URL_UNKNOWN
};

typedef enum URLTYPE URLTYPE;


int Command_fetch(apr_pool_t *p, const char *url, int fetch_only);

int Command_install(apr_pool_t *p, const char *url, const char *configure_opts,
	                const char *make_opts, const char *install_opts);

int Command_depends(apr_pool_t *p, const char *path);

int Command_build(apr_pool_t *p, const char *url, const char *configure_opts,
   					const char *make_opts, const char *install_opts);

URLTYPE Get_urlt(const char *path);

#endif
