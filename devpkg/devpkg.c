/*
 * =====================================================================================
 *
 *       Filename:  devpkg.c
 *
 *    Description:  main file for devpkg
 *
 *        Version:  1.0
 *        Created:  12/14/2012 10:36:55 AM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Ji.Zhilong
 *   Organization:  
 *
 * =====================================================================================
 */
#include <stdlib.h>
#include <apr_general.h>
#include <apr_getopt.h>
#include <apr_strings.h>
#include <apr_lib.h>

#include "dbg.h"
#include "db.h"
#include "commands.h"


void print_help_info()
{
  printf("devpackage v1.0\n");
  printf("type devpackage -h for this info.\n");
  printf("USAGE:devpackage -[option] [arg].\n");
  printf("options:\n");
  printf("-I\tINSTALL\n");
  printf("-L\tLIST\n");
  printf("-c\tCONFIG\n");
  printf("-m\tMAKE\n");
  printf("-i\tinstall\n");
  printf("-S\tSETUP\n");
  printf("-F\tFETCH\n");
  printf("-B\tBUILD\n");
  printf("-r\tRESET DB\n");

}

int main(int argc, const char *argv[])
{
  apr_pool_t *p = NULL;
  apr_pool_initialize();
  apr_pool_create(&p, NULL);

  apr_getopt_t *opt;
  apr_status_t rv;

  char ch = '\0';
  const char *optarg = NULL;
  const char *config_opts = NULL;
  const char *install_opts = NULL;
  const char *make_opts = NULL;
  const char *url = NULL;
  enum CommandType request = COMMAND_DONE;

  rv = apr_getopt_init(&opt, p, argc, argv);

  while(apr_getopt(opt, "I:Lc:m:i:d:SF:B:hH:r", &ch, &optarg) == APR_SUCCESS) {
	switch(ch) {
	  case 'I':
		request = COMMAND_INSTALL;
		url = optarg;
		break;

	  case 'L':
		request = COMMAND_LIST;
		break;

	  case 'c':
		config_opts = optarg;
		break;

	  case 'm':
		make_opts = optarg;
		break;

	  case 'i':
		install_opts = optarg;
		break;

	  case 'S':
		request = COMMAND_INIT;
		break;

	  case 'F':
		request = COMMAND_FETCH;
		url = optarg;
		break;

	  case 'B':
		request = COMMAND_BUILD;
		url = optarg;
		break;

	  case 'r':
		request = COMMAND_RESET;
		break;
		
	  case 'h':
	  case 'H':
		print_help_info();
		return 0;
		break;
	}
  }

  switch(request) {
	case COMMAND_INSTALL:
	  check(url, "You must at leasty give a URL.");
	  Command_install(p, url, config_opts, make_opts, install_opts);
	  break;

	case COMMAND_LIST:
	  DB_list();
	  break;

	case COMMAND_FETCH:
	  check(url != NULL, "You must give a URL.");
	  Command_fetch(p, url, 1);
	  log_info("Downloaded to %s and in /tmp/", BUILD_DIR);
	  break;

	case COMMAND_BUILD:
	  check(url, "You must at least give a URL.");
	  Command_build(p, url, config_opts, make_opts, install_opts);
	  break;

	case COMMAND_INIT:
	  rv = DB_init();
	  check(rv == 0, "Failed to make the database.");
	  break;

	case COMMAND_RESET:
	  rv = DB_reset();
	  check(rv == 0, "Failed to reset the database.");
	  break;

	default:
	  print_help_info();
	  sentinel("Invalid command given.");

  }

  return 0;

error:
  return 1;
}


