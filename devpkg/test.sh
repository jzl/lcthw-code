#!/bin/sh
set -e

rm -rf /usr/local/.devpkg

TARGZ=http://127.0.0.1:8000/ldevpkg.tar.gz
TARBZ2=http://127.0.0.1:8000/ldevpkg.tar.bz2
GIT=/tmp/ldevpkg.git

# do a setup
./devpkg -S

# regular tar.gz
./devpkg -I $TARGZ

# regular tar.bz2
./devpkg -I $TARBZ2

# regular .git
./devpkg -I $GIT


# list out what's installed
./devpkg -L

# manually fetch/build if already installed
#./devpkg -F https://github.com/zedshaw/srpmin.git

#./devpkg -B https://github.com/zedshaw/srpmin.git

## manually fetch/build it not installed
#./devpkg -F http://mirror.its.uidaho.edu/pub/apache//apr/apr-util-1.3.12.tar.gz

#./devpkg -B http://mirror.its.uidaho.edu/pub/apache//apr/apr-util-1.3.12.tar.gz -c "--with-apr=/usr/local/apr"

## idempotent build test
#./devpkg -I http://mirror.its.uidaho.edu/pub/apache//apr/apr-util-1.3.12.tar.gz -c "--with-apr=/usr/local/apr"


## just run the depends, nothing should build
#./devpkg -I ./DEPENDS
