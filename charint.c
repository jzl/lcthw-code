#include <stdio.h>

int main()
{
  int i = 0;
  char c[] = {'\0','\1','\0','\0'};

  printf("i before assignment:%d\n",i);
  i = *(int *)&c[0];
  printf("i after assignment:%d\n",i);
  printf("c each: %c %c %c %c\n",
	      c[0],c[1],c[2],c[3]);

  return 0;
}

